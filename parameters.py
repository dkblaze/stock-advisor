import os
import time
from tensorflow.keras.layers import LSTM

def model_namev(date_now,ticker,loss,optimizer,cell,nSteps,nDays,nLayers,units,bidirectional):
    if bidirectional:
        return f"{date_now}_{ticker}-{loss}-{optimizer}-{cell.__name__}-seq-{nSteps}-step-{nDays}-layers-{nLayers}-units-{units}-bidirectional"
    else:
       return f"{date_now}_{ticker}-{loss}-{optimizer}-{cell.__name__}-seq-{nSteps}-step-{nDays}-layers-{nLayers}-units-{units}"

nSteps = 160
nDays = 7
test_size = 0.25
featureColumns = ["adjclose", "volume", "open", "high", "low"]
date_now = time.strftime("%Y-%m-%d")
nLayers = 4
cell = LSTM
units = 256
dropout = 0.4
bidirectional = True
loss = "huber_loss"
optimizer = "Nadam"
batch_size = 81
epochs = 900





