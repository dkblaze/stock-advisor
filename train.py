from stockPrediction import createModel, loadData
from tensorflow.keras.layers import LSTM
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
import os
import pandas as pd
from parameters import *
from getCompaniesList import *

if not os.path.isdir("results"):
    os.mkdir("results")
if not os.path.isdir("logs"):
    os.mkdir("logs")
if not os.path.isdir("data"):
    os.mkdir("data")

companies = ["OPK"]
#temp = Companies().getCompaniesSymbol()
#companies = temp["Symbol"]
for ticker in companies:

    ticker_data_filename = os.path.join("data", f"{ticker}_{date_now}.csv")
    data = loadData(ticker, nSteps, nDays=nDays, test_size=test_size, featureColumns=featureColumns)
    data["df"].to_csv(ticker_data_filename)
    model = createModel(nSteps, loss=loss, units=units, cell=cell, nLayers=nLayers,dropout=dropout, optimizer=optimizer, bidirectional=bidirectional)
    checkpointer = ModelCheckpoint(os.path.join("results", model_namev(date_now,ticker,loss,optimizer,cell,nSteps,nDays,nLayers,units,bidirectional) + ".h5"), save_weights_only=True, save_best_only=True, verbose=1)
    tensorboard = TensorBoard(log_dir=os.path.join("logs", model_namev(date_now,ticker,loss,optimizer,cell,nSteps,nDays,nLayers,units,bidirectional)))
    history = model.fit(data["X_train"], data["y_train"],batch_size=batch_size,epochs=epochs, validation_data=(data["X_test"], data["y_test"]), callbacks=[checkpointer, tensorboard],verbose=1)

    model.save(os.path.join("results", model_namev(date_now,ticker,loss,optimizer,cell,nSteps,nDays,nLayers,units,bidirectional)) + ".h5")


