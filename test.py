from stockPrediction import createModel, loadData, np
from parameters import *
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from getCompaniesList import *

def plotGraph(model, data):

    y_test = data["y_test"]
    X_test = data["X_test"]
    y_pred = model.predict(X_test)
    y_test = np.squeeze(data["column_scaler"]["adjclose"].inverse_transform(np.expand_dims(y_test, axis=0)))
    y_pred = np.squeeze(data["column_scaler"]["adjclose"].inverse_transform(y_pred))
    plt.plot(y_test[-200:], c='b')
    plt.plot(y_pred[-200:], c='r')
    plt.xlabel("Days")
    plt.ylabel("Price")
    plt.legend(["Actual Price", "Predicted Price"])
    plt.show()


def getAccuracy(model, data):

    y_test = data["y_test"]
    X_test = data["X_test"]
    y_pred = model.predict(X_test)
    y_test = np.squeeze(data["column_scaler"]["adjclose"].inverse_transform(np.expand_dims(y_test, axis=0)))
    y_pred = np.squeeze(data["column_scaler"]["adjclose"].inverse_transform(y_pred))
    y_pred = list(map(lambda current, future: int(float(future) > float(current)), y_test[:-nDays], y_pred[nDays:]))
    y_test = list(map(lambda current, future: int(float(future) > float(current)), y_test[:-nDays], y_test[nDays:]))

    return accuracy_score(y_test, y_pred)


def predict(model, data, classification=False):
    
    last_sequence = data["last_sequence"][:nSteps]
    column_scaler = data["column_scaler"]
    last_sequence = last_sequence.reshape((last_sequence.shape[1], last_sequence.shape[0]))
    last_sequence = np.expand_dims(last_sequence, axis=0)
    prediction = model.predict(last_sequence)
    predicted_price = column_scaler["adjclose"].inverse_transform(prediction)[0][0]

    return predicted_price


companies = ["OPK"]
#temp = Companies().getCompaniesSymbol()
#companies = temp["Symbol"]

for ticker in companies:
    data = loadData(ticker, nSteps, nDays=nDays, test_size=test_size,featureColumns=featureColumns, shuffle=False)
    model = createModel(nSteps, loss=loss, units=units, cell=cell, nLayers=nLayers,dropout=dropout, optimizer=optimizer, bidirectional=bidirectional)
    model_path = os.path.join("results", model_namev(date_now,ticker,loss,optimizer,cell,nSteps,nDays,nLayers,units,bidirectional)) + ".h5"
    model.load_weights(model_path)
    mse, mae = model.evaluate(data["X_test"], data["y_test"], verbose=0)
    mean_absolute_error = data["column_scaler"]["adjclose"].inverse_transform([[mae]])[0][0]
    print(f"Stock symbol {ticker}")
    print("Mean Absolute Error:", mean_absolute_error)
    future_price = predict(model, data)
    print(f"Future price after {nDays} days is {future_price:.2f}$")
    print("Accuracy Score:", getAccuracy(model, data))
    print("-----------------------------------------------------------")
    plotGraph(model, data)