import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout, Bidirectional
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from yahoo_fin import stock_info as si
from collections import deque

import numpy as np
import pandas as pd
import random

np.random.seed(314)
tf.random.set_seed(314)
random.seed(314)


def loadData(ticker, nSteps=50, scale=True, shuffle=True, nDays=1,test_size=0.2, featureColumns=['adjclose', 'volume', 'open', 'high', 'low']):
   
    if isinstance(ticker, str):
        df = si.get_data(ticker)
    elif isinstance(ticker, pd.DataFrame):
        df = ticker
    else:
        raise TypeError("ticker format not found")
 
    result = {}
    result['df'] = df.copy()
    for col in featureColumns:
        assert col in df.columns, f"'{col}' doesn't exist in the df."

    if scale:
        column_scaler = {}
        for column in featureColumns:
            scaler = preprocessing.MinMaxScaler()
            df[column] = scaler.fit_transform(np.expand_dims(df[column].values, axis=1))
            column_scaler[column] = scaler
        result["column_scaler"] = column_scaler

    df['future'] = df['adjclose'].shift(-nDays)
    last_sequence = np.array(df[featureColumns].tail(nDays))
    df.dropna(inplace=True)
    sequence_data = []
    sequences = deque(maxlen=nSteps)

    for entry, target in zip(df[featureColumns].values, df['future'].values):
        sequences.append(entry)
        if len(sequences) == nSteps:
            sequence_data.append([np.array(sequences), target])

    last_sequence = list(sequences) + list(last_sequence)
    last_sequence = np.array(pd.DataFrame(last_sequence).shift(-1).dropna())
    result['last_sequence'] = last_sequence
    
    X, y = [], []
    for seq, target in sequence_data:
        X.append(seq)
        y.append(target)
    X = np.array(X)
    y = np.array(y)
    X = X.reshape((X.shape[0], X.shape[2], X.shape[1]))
    result["X_train"], result["X_test"], result["y_train"], result["y_test"] = train_test_split(X, y, test_size=test_size, shuffle=shuffle)

    return result


def createModel(sequence_length, units=256, cell=LSTM, nLayers=2, dropout=0.3,loss="mean_absolute_error", optimizer="rmsprop", bidirectional=False):

    model = Sequential()
    for i in range(nLayers):

        if i == 0:
            if bidirectional:
                model.add(Bidirectional(cell(units, return_sequences=True), input_shape=(None, sequence_length)))
            else:
                model.add(cell(units, return_sequences=True, input_shape=(None, sequence_length)))

        elif i == nLayers - 1:
            if bidirectional:
                model.add(Bidirectional(cell(units, return_sequences=False)))
            else:
                model.add(cell(units, return_sequences=False))

        else:
            if bidirectional:
                model.add(Bidirectional(cell(units, return_sequences=True)))
            else:
                model.add(cell(units, return_sequences=True))

        model.add(Dropout(dropout))
        
    model.add(Dense(1, activation="linear"))
    model.compile(loss=loss, metrics=["mean_absolute_error"], optimizer=optimizer)

    return model

