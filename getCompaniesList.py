# Z7C85XR5JKYI27VY
import csv
from collections import defaultdict
from priceAnalysis import Analysis
from sentiment import NewsClient



class Companies:

    def stocksList(self,keyword):

        resultScore = 0
        scoreRequest = NewsClient()
        resultScore = scoreRequest.getNewsWeekly(keyword=keyword)

        return resultScore


    def scoreCompanies(self):

        temp = Analysis().companiesResult(amount=5000)
        result = {"Name":[],"Score":[]}
        for items in temp:
            result["Score"].append(self.stocksList(items))
            result["Name"].append(items)

        return result
    

    def topResults(self):

        data = self.scoreCompanies()
        results= {"Name":[],"Score":[]}
    
        for (index,value) in enumerate(data["Score"]):
            if len(results["Score"])<3:
                results["Score"].append(value)
                results["Name"].append(data["Name"][index])
            for (index2,value2) in enumerate(results["Score"]):
                    if value > value2 and value not in results["Score"]:
                        results["Score"][index2] = value
                        results["Name"][index2] = results["Name"][index2]

        return results
    

    def readDatabase(self,database):

        stockList = defaultdict(list)
        with open(database) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for(k,v) in row.items():
                    stockList[k].append(v)

        return stockList
    

    def getCompaniesSymbol(self):

        firstDB = self.readDatabase("companylist.csv")
        topCompanies = self.topResults()
        result = {"Name":[],"Score":[],"Symbol":[]}
        for (index,value) in enumerate(topCompanies["Name"]):
            for (index2,value2) in enumerate(firstDB["Name"]):
                if value2 == value and value not in result["Name"]:
                    result["Name"].append(value)
                    result["Score"].append(topCompanies["Score"][index])
                    result["Symbol"].append(firstDB["Symbol"][index2])
                    
        return result
    
                    



test = Companies()
print(test.getCompaniesSymbol())  


    
    


