import re
from datetime import datetime, timedelta
from newsapi import NewsApiClient
from textblob import TextBlob

class NewsClient():

    @staticmethod
    def clean_news(news):

        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])| (\w+:\ / \ / \S+)", " ", news).split())


    def get_news_sentiment(self, news):

        analysis = TextBlob(self.clean_news(news))
        if analysis.sentiment.polarity > 0.5:
            return 'positive'
        elif analysis.sentiment.polarity == 0:
            return 'neutral'
        else:
            return 'negative'


    def get_news(self, keyword):

        news_list = []
        api_key = "b2462f5a29024576a33ff32853c64ea0"
        newsapi = NewsApiClient(api_key=api_key)
        fetched_news = newsapi.get_top_headlines(q= "bitcoin")
        articles = fetched_news['articles']
        for each in articles:
            parsed_news = {}
            parsed_news['text'] = each['title']
            parsed_news['sentiment'] = self.get_news_sentiment(each['title'])
            if parsed_news not in news_list:
                news_list.append(parsed_news)

        return news_list


    def getNewsWeekly(self,keyword):
        #b2462f5a29024576a33ff32853c64ea0
        #aae79ada8019430384ae741e83d7ed40
        news_list = []
        score = 0
        sentimentScore = {"neutral":0, "positive":0, "negative":0 }
        api_key = "b2462f5a29024576a33ff32853c64ea0"
        newsapi = NewsApiClient(api_key=api_key)
        fetchedNews = newsapi.get_everything(q=keyword,from_param= datetime.today() - timedelta(days= 7),to=datetime.today())
        articles = fetchedNews["articles"]

        for each in articles:
            parsed_news = {}
            parsed_news['text'] = each['title']
            parsed_news['sentiment'] = self.get_news_sentiment(each['title'])
            
            if parsed_news['sentiment'] == "neutral":
                sentimentScore['neutral']+=1
            elif parsed_news['sentiment'] == "positive":
                sentimentScore['positive']+=1
            elif parsed_news['sentiment'] == "negative":
                sentimentScore['negative']+=1
            if parsed_news not in news_list:
                news_list.append(parsed_news)
                
        return sentimentScore["neutral"] + 0.5*sentimentScore["positive"] -0.5*sentimentScore["negative"]

    
test = NewsClient().getNewsWeekly("Tesla")
print(test)