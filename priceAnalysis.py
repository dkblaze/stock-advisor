import csv
from collections import defaultdict
import time

class Analysis():

    def readClosePriceData(self):

        stockList = defaultdict(list)
        with open("closePrices.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for(k,v) in row.items():
                    stockList[k].append(v)

        return stockList


    def readCompanyData(self):

        stockList = defaultdict(list)
        with open("companylist.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for(k,v) in row.items():
                    stockList[k].append(v)

        return stockList


    def pickStock(self,amount):
        
        closePrice = Analysis().readClosePriceData()
        result = {"Symbol":[],"Close":[]}
        for (index,value) in enumerate(closePrice["Symbol"]):
            if amount/float(closePrice["Close"][index])>50000.0:
                result["Symbol"].append(value)
                result["Close"].append(closePrice["Close"][index])

        return result


    def companiesResult(self,amount):
        
        resultList = Analysis().pickStock(amount=amount)
        test2 = Analysis().readCompanyData()
        final = []
        for value in resultList["Symbol"]:
            for (index,value2) in enumerate(test2["Symbol"]):
                if value == value2:
                    final.append(test2["Name"][index])
                    
        return final




